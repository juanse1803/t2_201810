package test;

import controller.Controller;
import junit.framework.TestCase;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.vo.Taxi;

public class ListaTest extends TestCase {
	LinkedList<Taxi> lista;
	LinkedList lista2;
	
	 private void setupEscenario1( )
	    {
	        lista=new Lista<Taxi>();
	        
	    }
	 private void setupEscenario2() 
	 {
		 setupEscenario1();
		 Controller.loadServices();
		 lista=Controller.getTaxisOfCompany("Independiente");
	 }
	 private void setupEscenario3() 
	 {
		 lista2=new Lista();
		 Controller.loadServices();
		 lista2=Controller.getTaxisOfCompany("Independiente");
	 } 
	 public void testlistaVacia () 
	 {
		 setupEscenario1( );

	        assertEquals( "Al inicio no hay elementos en la lista", 0, lista.dartamanio() );
	        	    	 
	 }
	 public void testagregarElementos() 
	 {
	setupEscenario2();
	  assertEquals( "Hay elementos en la lista", 936, lista.dartamanio() );
	 }
	 
	 public void testeliminarElementos() {
		 setupEscenario3();
		
		 Object prueba=lista2.darElemento(2);
		 lista2.eliminar(lista2.darElemento(1));
		 assertEquals( "Hay un elemento menos en la lista", 935, lista2.dartamanio() );
		 
		 assertEquals( "no deberia existir este elemento", prueba, lista2.darElemento(1) );
		 
	 }

	 public void testAgregarFinalElementos() 
	 {
		 setupEscenario2();
	Taxi nuevo= new Taxi("123442433","JUANSE");
	lista.agregarFinal(nuevo);
	assertEquals("No coninciden los datos","123442433",lista.darElemento(lista.dartamanio()-1).getTaxiId());
	 }
	
	 public void testAgregarPrincipioElementos() 
	 {
		 setupEscenario2();
			Taxi nuevo= new Taxi("123442433","JUANSE");
			lista.agregarPrincipio(nuevo);
			assertEquals("No coninciden los datos","123442433",lista.darElemento(0).getTaxiId());
			 
	 }
	 
}
