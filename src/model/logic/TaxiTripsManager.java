package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.Lista;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private ArrayList<Taxi> taxis= new ArrayList();
	private LinkedList<Taxi> listaT= new Lista<Taxi>();
	
	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with " + serviceFile);
        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader(serviceFile));

            JSONArray array = (JSONArray) obj;
            Iterator itr2 = array.iterator();
            
            while (itr2.hasNext()) 
            {
            	 JSONObject jsonObject=(JSONObject)itr2.next();
            	 String taxi_id = (String) jsonObject.get("taxi_id");
                 String company = (String) jsonObject.get("company");
                 Taxi taxi=new Taxi(taxi_id,company);
                 boolean a=true;
                 int b=0;                 
                 for(int i=0;i<listaT.dartamanio()&& a==true;i++) {         	
                if(listaT.darElemento(i).compareTo(taxi)==0) 
              {
            	a=false;
                b=i;
              }
                }
                            	 
                  if(a==true) { 
               listaT.agregarPrincipio(taxi); 
                  }
                  String d1 = (String) jsonObject.get("dropoff_census_tract");
                  String d2= (String) jsonObject.get("dropoff_centroid_latitude");
                  JSONObject jod3 = null; 
                  double longitude=0;
                  double latitude =0;
                  if ( jsonObject.get("dropoff_centroid_location") != null )
  				{ jod3 =((JSONObject) jsonObject.get("dropoff_centroid_location"));
  				
  				  /* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
  				  JSONArray dropoff_localization_arr =(JSONArray) jod3.get("coordinates");
  				  
  				  /* Obtener cada coordenada del JsonArray como Double */
  				  longitude =(Double) dropoff_localization_arr.get(0);
  				 latitude = (Double)dropoff_localization_arr.get(1);
  				  
  				}
  				                 
                  String d4= (String) jsonObject.get("dropoff_centroid_longitude");
                  String d5 = (String) jsonObject.get("dropoff_community_area");
                  String d6 = (String) jsonObject.get("extras");
                  String d7 = (String) jsonObject.get("fare");
                  String d8 = (String) jsonObject.get("payment_type");
                  String d9 = (String) jsonObject.get("pickup_census_tract");
                  String d10 = (String) jsonObject.get("pickup_centroid_latitude");
                  
                  
                  JSONObject jod11 = null; 
                  double longituded11=0;
                  double latituded11 =0;
                  if ( jsonObject.get("pickup_centroid_location") != null )
    				{ jod11 =((JSONObject) jsonObject.get("pickup_centroid_location"));
    				
    				  /* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
    				  JSONArray dropoff_localization_arr = (JSONArray)jod11.get("coordinates");
    				  
    				  /* Obtener cada coordenada del JsonArray como Double */
    				  longituded11 =(Double) dropoff_localization_arr.get(0);
    				 latituded11 = (Double)dropoff_localization_arr.get(1);
    				  
    				}
                                
                  
                  String d12 = (String) jsonObject.get("pickup_centroid_longitude");
                  String d13 = (String) jsonObject.get("pickup_community_area");
                  String d14 = (String) jsonObject.get("tips");
                  String d15 = (String) jsonObject.get("tolls");
                  String d16 = (String) jsonObject.get("trip_end_timestamp");
                  String d17 = (String) jsonObject.get("trip_id");
                  String d18 = (String) jsonObject.get("trip_miles");
                  String d19 = (String) jsonObject.get("trip_seconds");
                  String d20 = (String) jsonObject.get("trip_start_timestamp");
                  String d21 = (String) jsonObject.get("trip_total");
                  listaT.darElemento(b).agregarServicio(d1, d2,latitude,longitude, d4, d5, d6,d7,d8,d9,d10,latituded11,longituded11,d12,d13,d14,d15,d16,d17,d18,d19,d20,d21); 	
             }
            System.out.println("el numero de taxis es: "+listaT.dartamanio());
            }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

		
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxisOfCompany with " + company);	
		   LinkedList devolver=new Lista();   
         for(int i=0;i<listaT.dartamanio();i++) {
         	
       
     	   
     		  if(listaT.darElemento(i).getCompany().equals(company)) 
     			{
     			devolver.agregarPrincipio(listaT.darElemento(i).getTaxiId());
     			  System.out.println(listaT.darElemento(i).getTaxiId());
     			}
     		
}
		
         
		return devolver;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		   LinkedList devolver=new Lista();   

		String comp=Integer.toString(communityArea);
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		LinkedList<Service> r=new Lista<Service>();
		
		for(int i=0;i<listaT.dartamanio();i++) {
			r=(listaT.darElemento(i).darServicios(comp));
			for(int j=0;j<r.dartamanio();j++) { 
				System.out.println(r.darElemento(j).getTripId());
				devolver.agregarPrincipio(r.darElemento(j));
			}
			}

		return devolver;
	}


}
