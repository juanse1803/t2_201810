package model.vo;

import java.util.ArrayList;

import model.data_structures.LinkedList;
import model.data_structures.Lista;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String company;
	private String taxi_id;
	private LinkedList<Service> listaS= new Lista();
	private Service servicio;
	
	
	public Taxi(String id,String pCompany)
	{
		taxi_id=id;
		if(pCompany==null) {
			company="Independiente";
		}
		else {
		company=pCompany;
	}
		}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	
	}

	public void agregarServicio(String d1,String d2,double d3la,double d3lo,String d4,String d5,String d6,String d7,String d8,String d9,String d10,double d11la,double d11lo,String d12,String d13,String d14,String d15,String d16,String d17,String d18,String d19,String d20,String d21) 
	{
		servicio= new Service(taxi_id,d1,d2,d3la,d3lo,d4,d5,d6,d7,d8,d9,d10,d11lo,d11la,d12,d13,d14,d15,d16,d17,d18,d19,d20,d21);
		listaS.agregarPrincipio(servicio);
		  
	}
	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	public LinkedList darServicios(String comp) 
	{
		LinkedList retorno=new Lista();
		for(int pos=0;pos<listaS.dartamanio();pos++) {
		
		if(listaS.darElemento(pos).getComunArea().compareTo(comp)==0)
		{
			retorno.agregarPrincipio(listaS.darElemento(pos));
		}
		}
		
		return retorno;
	}
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		
		return o.getTaxiId().equals(this.taxi_id)? 0:1;
	}	
}
