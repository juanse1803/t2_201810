package model.vo;

import java.util.ArrayList;





/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String taxi_id,dropoff_census_tract,payment_type, trip_id, trip_start_timestamp,trip_end_timestamp, trip_seconds,trip_miles, trip_total,dropoff_centroid_latitude, dropoff_centroid_longitude, dropoff_community_area, extras, fare,pickup_census_tract, pickup_centroid_latitud,pickup_centroid_longitude, pickup_community_area, tips, tolls;
	private double dropoff_centroid_location_lo,dropoff_centroid_location_la, pickup_centroid_location_lo,pickup_centroid_location_la;
	
	public Service(String id,String pD1,String pD2,double pD3la,double pD3lo,String pD4,String pD5,String pD6,String pD7,String pD8,String pD9,String pD10,double pD11lo,double pD11la,String pD12,String pD13,String pD14,String pD15, String pD16,String pD17,String pD18,String pD19,String pD20,String pD21) 
	{
		dropoff_census_tract=pD1;
		dropoff_centroid_latitude=pD2;
		dropoff_centroid_location_la=pD3la;
		dropoff_centroid_location_lo=pD3lo;
		dropoff_centroid_longitude=pD4;
		dropoff_community_area=pD5;
		extras=pD6;
		fare=pD7;
		payment_type=pD8;
		pickup_census_tract=pD9;
		pickup_centroid_latitud=pD10;
		pickup_centroid_location_la=pD11la;
		pickup_centroid_location_lo=pD11lo;
		pickup_centroid_longitude=pD12;
		pickup_community_area=pD13;
		taxi_id=id;
		tips=pD14;
		tolls=pD15;
		trip_end_timestamp=pD16;
		trip_id=pD17;
		trip_miles=pD18;
		trip_seconds=pD19;
		trip_start_timestamp=pD20;
		trip_total=pD21;		
		
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds!=null ? Integer.parseInt(trip_seconds):0;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return(trip_miles!=null )? Double.parseDouble(trip_miles):0;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total!=null?Double.parseDouble(trip_total):0;
	}
	public String getComunArea() {
		// TODO Auto-generated method stub
		return dropoff_community_area!=null?dropoff_community_area:"";
	}
	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return o.getTripId().equals(trip_id)? 0:1;
	}
}
