package model.data_structures;

import java.util.Iterator;

public class Lista <T extends Comparable<T>> implements LinkedList<T> {

	private Node<T> cabeza;
	private Node<T> cola;
	
	private int tamanio;
	
	public Lista() {
		cabeza = new Node<>(null, null, null);
		cola = new Node<>(null, cabeza, null);
		cabeza.agregarSiguiente(cola);
		tamanio=0;
	}
	
	

	@Override
	public void eliminar(T elemento) {
		// TODO Auto-generated method stub
		 boolean termine = false;

         Node <T> nodo = darPrimero();

         while ( !termine &&nodo.darSiguiente() != cola )

         {

                         if( nodo.darNodo().equals(elemento))

                         {

                                         termine = true;

                         }else
nodo=nodo.darSiguiente();
         }

         Node<T> anterior = nodo.darAnterior();

         Node<T> siguiente = nodo.darSiguiente();

         anterior.agregarSiguiente(siguiente);

         siguiente.agregarAnterior(anterior);

         tamanio--;
         
         
	}


	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		Node<T> a= cabeza.darSiguiente();
		for(int i=1;i<=pos;i++)
		{
			a=a.darSiguiente();
		}
	
		return  a.darNodo();
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void agregarPrincipio(T elemento) {
		// TODO Auto-generated method stub
		agregarEntre(cabeza,elemento,cabeza.darSiguiente());
		
	}

	@Override
	public void agregarFinal(T elemento) {
		// TODO Auto-generated method stub
		agregarEntre(cola.darAnterior(),elemento,cola);
	}

	@Override
	public void agregarEntre(Node<T> antes, T elemento, Node<T> despues) {
		// TODO Auto-generated method stub
		Node<T> newest = new Node<T>(elemento, antes, despues);
		despues.agregarAnterior(newest);
		antes.agregarSiguiente(newest);
		tamanio++;

	}



	@Override
	public int dartamanio() {
		// TODO Auto-generated method stub
		return tamanio;
	}



	@Override
	public Node<T> darPrimero() {
		// TODO Auto-generated method stub
		return cabeza.darSiguiente();
	}



	@Override
	public Node<T> darUltimo() {
		// TODO Auto-generated method stub
		return cola.darAnterior();
	}

}
