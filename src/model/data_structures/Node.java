package model.data_structures;


public class Node<T extends Comparable<T>> {
private Node<T> siguiente;
private Node<T> anterior;
private T elemento;
public Node (T pElemento,Node<T> pAnterior,Node<T> pSiguiente) 
{
siguiente=pSiguiente;
anterior=pAnterior;
elemento=pElemento;
}
public Node<T>darSiguiente()
{
	return siguiente;
}
public Node<T> darAnterior()
{
	return anterior;
	}
public void agregarSiguiente(Node<T> elemento){
this.siguiente=elemento;
}
public void agregarAnterior(Node<T> elemento){
this.anterior=elemento;
}
public T darNodo(){
	return elemento;
}
public void agregarItem(T elemento){
this.elemento=elemento;
}
}
